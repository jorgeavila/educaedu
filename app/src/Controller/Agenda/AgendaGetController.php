<?php

namespace Educaedu\Api\Controller\Agenda;

use Educaedu\Module\Agenda\Contract\Exception\AgendaEventsNotFound;
use Educaedu\Module\Agenda\Contract\Exception\EventDateNotValid;
use Educaedu\Module\Agenda\Contract\Query\AgendaEventsFind;
use FOS\RestBundle\View\View;
use MessageBus\Domain\MessageBus;
use Symfony\Component\HttpFoundation\Response;

final class AgendaGetController
{
    private $messageBus;

    public function __construct(MessageBus $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public function __invoke($date)
    {
        $query = new AgendaEventsFind($date);

        try {
            $response = $this->messageBus->ask($query);
        } catch (AgendaEventsNotFound $exception) {
            $response = View::create(null, Response::HTTP_NOT_FOUND);
        } catch (EventDateNotValid $exception) {
            $response = View::create(null, Response::HTTP_CONFLICT);
        }

        return $response;
    }
}
