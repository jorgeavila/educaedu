<?php

use Bazinga\Bundle\RestExtraBundle\BazingaRestExtraBundle;
use FOS\HttpCacheBundle\FOSHttpCacheBundle;
use FOS\RestBundle\FOSRestBundle;
use Hautelook\TemplatedUriBundle\HautelookTemplatedUriBundle;
use JMS\SerializerBundle\JMSSerializerBundle;
use Educaedu\Infrastructure\Symfony\Bundle\EducaeduBundle;
use MessageBus\Infrastructure\Symfony\MessageBusBundle;
use Sensio\Bundle\DistributionBundle\SensioDistributionBundle;
use Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle;
use Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle;
use SimpleBus\SymfonyBridge\SimpleBusCommandBusBundle;
use SimpleBus\SymfonyBridge\SimpleBusEventBusBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\MonologBundle\MonologBundle;
use Symfony\Bundle\TwigBundle\TwigBundle;
use Symfony\Bundle\WebProfilerBundle\WebProfilerBundle;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            new FrameworkBundle(),
            new SensioFrameworkExtraBundle(),
            new TwigBundle(),

            new MonologBundle(),

            new FOSRestBundle(),
            new JMSSerializerBundle(),
            new HautelookTemplatedUriBundle(),
            new BazingaRestExtraBundle(),
            new FOSHttpCacheBundle(),

            new EducaeduBundle(),
            new SimpleBusCommandBusBundle(),
            new SimpleBusEventBusBundle(),
            new MessageBusBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'])) {
            $bundles[] = new WebProfilerBundle();
            $bundles[] = new SensioDistributionBundle();
            $bundles[] = new SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
