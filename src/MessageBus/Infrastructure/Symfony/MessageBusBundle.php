<?php

namespace MessageBus\Infrastructure\Symfony;

use MessageBus\Infrastructure\Symfony\DependencyInjection\MessageBusExtension;
use SimpleBus\SymfonyBridge\DependencyInjection\Compiler\RegisterHandlers;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MessageBusBundle extends Bundle
{
    private $configurationAlias;

    public function __construct($alias = 'message_bus')
    {
        $this->configurationAlias = $alias;
    }

    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(
            new RegisterHandlers(
                'message_bus.query_handler_map',
                'query_handler',
                'handles'
            )
        );
    }

    public function getContainerExtension()
    {
        return new MessageBusExtension($this->configurationAlias);
    }
}
