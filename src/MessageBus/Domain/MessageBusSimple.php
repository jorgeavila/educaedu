<?php

namespace MessageBus\Domain;

use MessageBus\Domain\Handler\Resolver\QueryHandlerResolver;
use SimpleBus\Message\Bus;

class MessageBusSimple implements MessageBus
{
    private $queryHandlerResolver;

    public function __construct(QueryHandlerResolver $queryHandlerResolver)
    {
        $this->queryHandlerResolver = $queryHandlerResolver;
    }

    public function ask(Query $query)
    {
        return $this->queryHandlerResolver->resolve($query)->handle($query);
    }
}
