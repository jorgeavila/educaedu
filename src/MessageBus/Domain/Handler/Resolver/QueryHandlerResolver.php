<?php

namespace MessageBus\Domain\Handler\Resolver;

use MessageBus\Domain\Handler\QueryHandler;
use MessageBus\Domain\Query;

interface QueryHandlerResolver
{
    /**
     * Resolve the QueryHandler for the given Query.
     *
     * @param Query $query
     *
     * @return QueryHandler
     */
    public function resolve(Query $query);
}
