<?php

namespace MessageBus\Domain\Handler\Resolver;

use MessageBus\Domain\Handler\Map\QueryHandlerMap;
use MessageBus\Domain\Name\QueryNameResolver;
use MessageBus\Domain\Query;

class NameBasedQueryHandlerResolver implements QueryHandlerResolver
{
    private $queryNameResolver;
    private $queryHandlers;

    public function __construct(QueryNameResolver $queryNameResolver, QueryHandlerMap $queryHandlers)
    {
        $this->queryNameResolver = $queryNameResolver;
        $this->queryHandlers     = $queryHandlers;
    }

    public function resolve(Query $query)
    {
        $name = $this->queryNameResolver->resolve($query);

        return $this->queryHandlers->handlerByQueryName($name);
    }
}
