<?php

namespace MessageBus\Domain\Handler\Map;

use MessageBus\Domain\Handler\Map\Exception\NoHandlerForQueryName;
use MessageBus\Domain\Handler\QueryHandler;

interface QueryHandlerMap
{
    /**
     * @param string $queryName
     *
     * @throws NoHandlerForQueryName
     *
     * @return QueryHandler
     */
    public function handlerByQueryName($queryName);
}
