<?php

namespace MessageBus\Domain\Handler;

use MessageBus\Domain\Query;
use MessageBus\Domain\Response;

interface QueryHandler
{
    /**
     * Handles the given query.
     *
     * @param Query $query
     *
     * @return Response
     */
    public function handle(Query $query);
}
