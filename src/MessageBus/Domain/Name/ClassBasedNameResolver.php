<?php

namespace MessageBus\Domain\Name;

use MessageBus\Domain\Query;

class ClassBasedNameResolver implements QueryNameResolver
{
    public function resolve(Query $query)
    {
        return get_class($query);
    }
}
