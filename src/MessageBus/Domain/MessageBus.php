<?php

namespace MessageBus\Domain;

interface MessageBus
{
    /**
     * @param Query $query
     *
     * @return Response
     */
    public function ask(Query $query);
}
