<?php

namespace Educaedu\Infrastructure\Symfony\Bundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

final class EducaeduBundle extends Bundle
{
}
