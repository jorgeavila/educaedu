<?php

namespace Educaedu\Test\Stub;

final class UuidStub
{
    public static function random()
    {
        return StubCreator::faker()->uuid;
    }
}
