<?php

namespace Educaedu\Module\Agenda\Infrastructure\Kulturklik;

use DateTimeImmutable;
use Educaedu\Module\Agenda\Test\AgendaModuleFunctionalTestCase;
use Educaedu\Module\Agenda\Test\Stub\AgendaEventStub;
use Educaedu\Module\Agenda\Test\Stub\EventDateStub;

final class AgendaEventsKulturklikRepositoryTest extends AgendaModuleFunctionalTestCase
{
    /** @var  AgendaEventsKulturklikRepository */
    private $repository;

    protected function setUp()
    {
        parent::setUp();

        $this->repository = new AgendaEventsKulturklikRepository();
    }

    /** @test */
    public function it_should_find_agenda_events_for_a_date_repository()
    {
        $eventDate   = EventDateStub::create(new DateTimeImmutable('2015-09-30'));
        $agendaEvent = AgendaEventStub::validForSeptember();

        $this->assertContains(
            $agendaEvent,
            $this->repository->searchByDate($eventDate),
            'Event in response',
            false,
            false
        );
    }

    /** @test */
    public function it_should_return_null_finding_agenda_events_for_a_date_in_the_past()
    {
        $eventDate = EventDateStub::create(new DateTimeImmutable('2039-09-30'));

        $this->assertNull($this->repository->searchByDate($eventDate));
    }
}
