<?php

namespace Educaedu\Module\Agenda\Tests\Behaviour;

use Educaedu\Module\Agenda\Application\QueryHandler\AgendaEventsFindQueryHandler;
use Educaedu\Module\Agenda\Application\Service\AgendaEventsFinder;
use Educaedu\Module\Agenda\Contract\Exception\AgendaEventsNotFound;
use Educaedu\Module\Agenda\Contract\Exception\EventDateNotValid;
use Educaedu\Module\Agenda\Contract\Test\Stub\AgendaEventResponseStub;
use Educaedu\Module\Agenda\Contract\Test\Stub\AgendaEventsResponseStub;
use Educaedu\Module\Agenda\Test\AgendaModuleUnitTestCase;
use Educaedu\Module\Agenda\Test\Stub\AgendaEventsFindStub;
use Educaedu\Module\Agenda\Test\Stub\AgendaEventStub;
use Educaedu\Module\Agenda\Test\Stub\EventDateStub;
use Educaedu\Module\Agenda\Test\Stub\EventNameStub;
use Educaedu\Module\Agenda\Test\Stub\EventTypeStub;
use Educaedu\Module\Agenda\Test\Stub\EventUrlStub;

final class AgendaEventsFindTest extends AgendaModuleUnitTestCase
{
    /** @var AgendaEventsFindQueryHandler */
    private $handler;

    protected function setUp()
    {
        parent::setUp();

        $finder        = new AgendaEventsFinder($this->agendaEventsRepository());
        $this->handler = new AgendaEventsFindQueryHandler($finder);
    }

    /** @test */
    public function it_should_find_events_for_a_date()
    {
        $query = AgendaEventsFindStub::random();

        $eventDate        = EventDateStub::fromString($query->date());
        $eventName        = EventNameStub::random();
        $eventType        = EventTypeStub::random();
        $eventUrl         = EventUrlStub::random();
        $agendaEvent      = AgendaEventStub::create($eventName, $eventType, $eventUrl);
        $expectedResponse =
            AgendaEventsResponseStub::create([AgendaEventResponseStub::create($eventName, $eventType, $eventUrl)]);

        $this->shouldSearchAgendaEvents($eventDate, [$agendaEvent]);

        $this->assertEquals($expectedResponse, $this->handler->handle($query));
    }

    /** @test */
    public function it_should_throw_an_exception_finding_a_events_for_a_date_without_events()
    {
        $this->setExpectedException(AgendaEventsNotFound::class);

        $query     = AgendaEventsFindStub::random();
        $eventDate = EventDateStub::fromString($query->date());

        $this->shouldSearchAgendaEvents($eventDate);

        $this->handler->handle($query);
    }

    /**
     * @test
     * @dataProvider invalidDates
     */
    public function it_should_throw_an_exception_finding_events_with_an_invalid_date($date)
    {
        $this->setExpectedException(EventDateNotValid::class);

        $query = AgendaEventsFindStub::create($date);

        $this->handler->handle($query);
    }

    public function invalidDates()
    {
        return [
            'null'          => ['date' => null],
            'invalidFormat' => ['date' => '30-09-2011'],
        ];
    }
}
