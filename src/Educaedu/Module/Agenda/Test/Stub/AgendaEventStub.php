<?php

namespace Educaedu\Module\Agenda\Test\Stub;

use Educaedu\Module\Agenda\Domain\AgendaEvent;
use Educaedu\Module\Agenda\Domain\EventName;
use Educaedu\Module\Agenda\Domain\EventType;
use Educaedu\Module\Agenda\Domain\EventUrl;

final class AgendaEventStub
{
    public static function create(EventName $name, EventType $type, EventUrl $url)
    {
        return new AgendaEvent($name, $type, $url);
    }

    public static function random()
    {
        return self::create(EventNameStub::random(), EventTypeStub::random(), EventUrlStub::random());
    }

    public static function validForSeptember()
    {
        return self::create(
            EventNameStub::create('Euskal Herriko Meatzaritzaren Museoa (Erakusketa iraunkorra)'),
            EventTypeStub::create('Erakusketa'),
            EventUrlStub::create(
                'http://www.kulturklik.euskadi.net/?lang=eu&p=476026&utm_source=agenda&utm_medium=api&utm_campaign=suscripciones-agenda'
            )
        );
    }
}
