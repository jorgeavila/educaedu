<?php

namespace Educaedu\Module\Agenda\Test\Stub;

use DateTimeImmutable;
use Educaedu\Module\Agenda\Contract\Query\AgendaEventsFind;

final class AgendaEventsFindStub
{
    public static function create($date)
    {
        return new AgendaEventsFind($date);
    }

    public static function random()
    {
        return self::create((new DateTimeImmutable())->format('Y-m-d'));
    }
}
