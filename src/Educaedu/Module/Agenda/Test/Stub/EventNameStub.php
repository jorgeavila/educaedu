<?php

namespace Educaedu\Module\Agenda\Test\Stub;

use Educaedu\Module\Agenda\Domain\EventName;
use Educaedu\Test\Stub\StubCreator;

final class EventNameStub
{
    public static function create($name)
    {
        return new EventName($name);
    }

    public static function random()
    {
        return self::create(StubCreator::faker()->md5);
    }
}
