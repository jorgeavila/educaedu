<?php

namespace Educaedu\Module\Agenda\Test\Stub;

use DateTimeImmutable;
use Educaedu\Module\Agenda\Domain\EventDate;

final class EventDateStub
{
    public static function create(DateTimeImmutable $date)
    {
        return new EventDate($date);
    }

    public static function random()
    {
        return self::create((new DateTimeImmutable()));
    }

    public static function fromString($date)
    {
        return self::create(DateTimeImmutable::createFromFormat('Y-m-d', $date));
    }
}
