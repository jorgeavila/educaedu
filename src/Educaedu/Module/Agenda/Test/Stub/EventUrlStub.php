<?php

namespace Educaedu\Module\Agenda\Test\Stub;

use Educaedu\Module\Agenda\Domain\EventUrl;
use Educaedu\Test\Stub\StubCreator;

final class EventUrlStub
{
    public static function create($url)
    {
        return new EventUrl($url);
    }

    public static function random()
    {
        return self::create(StubCreator::faker()->url);
    }
}
