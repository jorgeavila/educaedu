<?php

namespace Educaedu\Module\Agenda\Test\Stub;

use Educaedu\Module\Agenda\Domain\EventType;
use Educaedu\Test\Stub\StubCreator;

final class EventTypeStub
{
    public static function create($type)
    {
        return new EventType($type);
    }

    public static function random()
    {
        return self::create(StubCreator::faker()->md5);
    }
}
