<?php

namespace Educaedu\Module\Agenda\Test;

use Educaedu\Module\Agenda\Domain\AgendaEventsRepository;
use Educaedu\Module\Agenda\Domain\EventDate;
use Educaedu\Test\PHPUnit\UnitTestCase;
use Mockery as m;
use Mockery\MockInterface;

abstract class AgendaModuleUnitTestCase extends UnitTestCase
{
    private $agendaEventsRepository;

    /** @return AgendaEventsRepository|MockInterface */
    protected function agendaEventsRepository()
    {
        return $this->agendaEventsRepository =
            $this->agendaEventsRepository ?: $this->mock(AgendaEventsRepository::class);
    }

    protected function shouldSearchAgendaEvents(EventDate $date, array $agendaEvents = null)
    {
        $this->agendaEventsRepository()
            ->shouldReceive('searchByDate')
            ->once()
            ->with(m::mustBe($date))
            ->andReturn($agendaEvents);
    }
}
