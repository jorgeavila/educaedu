<?php

namespace Educaedu\Module\Agenda\Test;

use Educaedu\Test\PHPUnit\FunctionalTestCase;

abstract class AgendaModuleFunctionalTestCase extends FunctionalTestCase
{
    protected function getKulturklikAgendaEventsRepository()
    {
        return $this->service('educaedu.agenda.infrastructure.agenda_repository');
    }
}
