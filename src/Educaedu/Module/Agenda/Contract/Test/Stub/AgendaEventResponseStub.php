<?php

namespace Educaedu\Module\Agenda\Contract\Test\Stub;

use Educaedu\Module\Agenda\Contract\Response\AgendaEventResponse;
use Educaedu\Module\Agenda\Domain\EventName;
use Educaedu\Module\Agenda\Domain\EventType;
use Educaedu\Module\Agenda\Domain\EventUrl;
use Educaedu\Module\Agenda\Test\Stub\EventNameStub;
use Educaedu\Module\Agenda\Test\Stub\EventTypeStub;
use Educaedu\Module\Agenda\Test\Stub\EventUrlStub;

final class AgendaEventResponseStub
{
    public static function create(EventName $name, EventType $type, EventUrl $url)
    {
        return new AgendaEventResponse($name->value(), $type->value(), $url->value());
    }

    public static function random()
    {
        return self::create(EventNameStub::random(), EventTypeStub::random(), EventUrlStub::random());
    }
}
