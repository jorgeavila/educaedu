<?php

namespace Educaedu\Module\Agenda\Contract\Test\Stub;

use Educaedu\Module\Agenda\Contract\Response\AgendaEventsResponse;

final class AgendaEventsResponseStub
{
    public static function create(array $events)
    {
        return new AgendaEventsResponse($events);
    }

    public static function random()
    {
        return self::create([AgendaEventResponseStub::random()]);
    }
}
