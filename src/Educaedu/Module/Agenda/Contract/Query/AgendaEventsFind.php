<?php

namespace Educaedu\Module\Agenda\Contract\Query;

use MessageBus\Domain\Query;

final class AgendaEventsFind implements Query
{
    private $date;

    public function __construct($date)
    {
        $this->date = $date;
    }

    public function date()
    {
        return $this->date;
    }
}
