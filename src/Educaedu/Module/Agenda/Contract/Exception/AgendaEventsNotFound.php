<?php

namespace Educaedu\Module\Agenda\Contract\Exception;

use DateTimeImmutable;
use DomainException;

final class AgendaEventsNotFound extends DomainException
{
    public function __construct(DateTimeImmutable $date)
    {
        parent::__construct(sprintf('Agenda events not found for date: <%s>', $date->format('Y-m-d')));

        $this->code = 'agenda_not_found';
    }
}
