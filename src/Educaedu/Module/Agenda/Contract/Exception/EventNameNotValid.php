<?php

namespace Educaedu\Module\Agenda\Contract\Exception;

use DomainException;

final class EventNameNotValid extends DomainException
{
    public function __construct($name)
    {
        parent::__construct(sprintf('Event name <%s> not valid', $name));

        $this->code = 'event_name_not_valid';
    }
}
