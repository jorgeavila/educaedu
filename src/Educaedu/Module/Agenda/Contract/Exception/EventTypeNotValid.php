<?php

namespace Educaedu\Module\Agenda\Contract\Exception;

use DomainException;

final class EventTypeNotValid extends DomainException
{
    public function __construct($type)
    {
        parent::__construct(sprintf('Event type <%s> not valid', $type));

        $this->code = 'event_type_not_valid';
    }
}
