<?php

namespace Educaedu\Module\Agenda\Contract\Exception;

use DomainException;

final class EventDateNotValid extends DomainException
{
    public function __construct($date)
    {
        parent::__construct(sprintf('Event date <%s> not valid', $date));

        $this->code = 'event_date_not_valid';
    }
}
