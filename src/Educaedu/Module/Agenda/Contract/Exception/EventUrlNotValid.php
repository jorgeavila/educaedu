<?php

namespace Educaedu\Module\Agenda\Contract\Exception;

use DomainException;

final class EventUrlNotValid extends DomainException
{
    public function __construct($url)
    {
        parent::__construct(sprintf('Event url <%s> not valid', $url));

        $this->code = 'event_url_not_valid';
    }
}
