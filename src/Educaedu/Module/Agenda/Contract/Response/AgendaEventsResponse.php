<?php

namespace Educaedu\Module\Agenda\Contract\Response;

final class AgendaEventsResponse
{
    private $events;

    public function __construct(array $events)
    {
        $this->events   = $events;
    }

    public function events()
    {
        return $this->events;
    }
}
