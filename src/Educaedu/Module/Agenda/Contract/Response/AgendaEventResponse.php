<?php

namespace Educaedu\Module\Agenda\Contract\Response;

use MessageBus\Domain\Response;

final class AgendaEventResponse implements Response
{
    private $name;
    private $type;
    private $url;

    public function __construct($name, $type, $url)
    {
        $this->name = $name;
        $this->type = $type;
        $this->url  = $url;
    }

    public function name()
    {
        return $this->name;
    }

    public function type()
    {
        return $this->type;
    }

    public function url()
    {
        return $this->url;
    }
}
