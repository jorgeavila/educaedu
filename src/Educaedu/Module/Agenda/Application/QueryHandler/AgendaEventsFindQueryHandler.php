<?php

namespace Educaedu\Module\Agenda\Application\QueryHandler;

use DateTimeImmutable;
use Educaedu\Module\Agenda\Application\Service\AgendaEventsFinder;
use Educaedu\Module\Agenda\Contract\Exception\AgendaEventsNotFound;
use Educaedu\Module\Agenda\Contract\Query\AgendaEventsFind;
use Educaedu\Module\Agenda\Contract\Response\AgendaEventsResponse;
use Educaedu\Module\Agenda\Domain\EventDate;
use MessageBus\Domain\Handler\QueryHandler;
use MessageBus\Domain\Query;

final class AgendaEventsFindQueryHandler implements QueryHandler
{
    private $finder;

    public function __construct(AgendaEventsFinder $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param AgendaEventsFind|Query $query
     *
     * @throws AgendaEventsNotFound
     *
     * @return AgendaEventsResponse
     */
    public function handle(Query $query)
    {
        $date = new EventDate(DateTimeImmutable::createFromFormat('Y-m-d', $query->date()));

        return $this->finder->__invoke($date);
    }
}
