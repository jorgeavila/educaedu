<?php

namespace Educaedu\Module\Agenda\Application\Service;

use Educaedu\Module\Agenda\Contract\Exception\AgendaEventsNotFound;
use Educaedu\Module\Agenda\Contract\Response\AgendaEventResponse;
use Educaedu\Module\Agenda\Contract\Response\AgendaEventsResponse;
use Educaedu\Module\Agenda\Domain\AgendaEventsRepository;
use Educaedu\Module\Agenda\Domain\EventDate;

final class AgendaEventsFinder
{
    private $repository;

    public function __construct(AgendaEventsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(EventDate $date)
    {
        $events = $this->repository->searchByDate($date);
        
        if (null === $events) {
            throw new AgendaEventsNotFound($date->value());
        }

        return $this->buildResponse($events);
    }

    private function buildResponse(array $events)
    {
        $eventsResponse = [];

        foreach ($events as $event) {
            array_push(
                $eventsResponse,
                new AgendaEventResponse($event->name()->value(), $event->type()->value(), $event->url()->value())
            );
        }

        return new AgendaEventsResponse($eventsResponse);
    }
}
