<?php

namespace Educaedu\Module\Agenda\Domain;

interface AgendaEventsRepository
{
    /**
     * @param EventDate $date
     *
     * @return AgendaEvent[]|null
     */
    public function searchByDate(EventDate $date);
}
