<?php

namespace Educaedu\Module\Agenda\Domain;

use DateTimeImmutable;
use Educaedu\Module\Agenda\Contract\Exception\EventDateNotValid;

final class EventDate
{
    private $value;

    public function __construct($value)
    {
        $this->guard($value);

        $this->value = $value;
    }

    public function value()
    {
        return $this->value;
    }

    private function guard($value)
    {
        if (empty($value) || !is_object($value) ||
            !DateTimeImmutable::createFromFormat('Y-m-d', $value->format('Y-m-d'))
        ) {
            throw new EventDateNotValid($value);
        }
    }
}
