<?php

namespace Educaedu\Module\Agenda\Domain;

use Educaedu\Module\Agenda\Contract\Exception\EventNameNotValid;

final class EventName
{
    private $value;

    public function __construct($value)
    {
        $this->guard($value);

        $this->value = $value;
    }

    public function value()
    {
        return $this->value;
    }

    private function guard($value)
    {
        if (empty($value) || !is_string($value)) {
            throw new EventNameNotValid($value);
        }
    }
}
