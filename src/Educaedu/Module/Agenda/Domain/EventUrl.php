<?php

namespace Educaedu\Module\Agenda\Domain;

use Educaedu\Module\Agenda\Contract\Exception\EventUrlNotValid;

final class EventUrl
{
    private $value;

    public function __construct($value)
    {
        $this->guard($value);

        $this->value = $value;
    }

    public function value()
    {
        return $this->value;
    }

    private function guard($value)
    {
        if (empty($value) || false === filter_var($value, FILTER_VALIDATE_URL)) {
            throw new EventUrlNotValid($value);
        }
    }
}
