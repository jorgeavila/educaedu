<?php

namespace Educaedu\Module\Agenda\Domain;

final class AgendaEvent
{
    private $name;
    private $type;
    private $url;

    public function __construct(EventName $name, EventType $type, EventUrl $url)
    {
        $this->name = $name;
        $this->type = $type;
        $this->url  = $url;
    }

    public function name()
    {
        return $this->name;
    }

    public function type()
    {
        return $this->type;
    }

    public function url()
    {
        return $this->url;
    }
}
