<?php

namespace Educaedu\Module\Agenda\Infrastructure\Kulturklik;

use Educaedu\Module\Agenda\Domain\AgendaEvent;
use Educaedu\Module\Agenda\Domain\AgendaEventsRepository;
use Educaedu\Module\Agenda\Domain\EventDate;
use Educaedu\Module\Agenda\Domain\EventName;
use Educaedu\Module\Agenda\Domain\EventType;
use Educaedu\Module\Agenda\Domain\EventUrl;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Subscriber\Retry\RetrySubscriber;

final class AgendaEventsKulturklikRepository implements AgendaEventsRepository
{
    public function searchByDate(EventDate $date)
    {
        $eventsResponse = $this->getClient()->get(
            sprintf(
                'http://www.kulturklik.euskadi.net/?api_call=events&from=%s&to=%s&lang=eu&lang=eu',
                $date->value()->format('Y-m-d'),
                $date->value()->format('Y-m-d')
            )
        )->json();

        return $eventsResponse['count'] > 0 ? $this->buildEvents($eventsResponse['eventos']) : null;
    }

    private function getClient()
    {
        $client = new Client();

        $client->setDefaultOption('headers', ['User-Agent' => 'educaedu-test']);

        $client->getEmitter()->attach(
            new RetrySubscriber(
                [
                    'filter' => RetrySubscriber::createChainFilter(
                        [
                            RetrySubscriber::createConnectFilter(),
                            RetrySubscriber::createStatusFilter(),
                        ]
                    ),
                ]
            )
        );

        return $client;
    }

    private function buildEvents($events)
    {
        $searchedEvents = [];

        foreach ($events as $event) {
            try {

                array_push(
                    $searchedEvents,
                    new AgendaEvent(
                        new EventName($event['evento_titulo']),
                        new EventType($event['evento_tipo'] ?: 'unknown'),
                        new EventUrl($event['evento_url'])
                    )
                );
            } catch (Exception $e){
                var_dump($event);die;

            }

        }

        return $searchedEvents;
    }
}
