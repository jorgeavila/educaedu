App Educaedu
=====================

1) Instalar las dependencias
----------------------
php composer.phar install -o


2) Ejecutar los Tests
----------------------

./bin/phpunit -c app/
./bin/behat


3) Iniciar un webserver
----------------------
php app/console server:start


4) Abrir html con eventos del día
----------------------
http://127.0.0.1:8000/view.html


