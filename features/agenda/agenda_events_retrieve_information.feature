Feature: Recover agenda events information
  As a user
  I want to recover agenda events for an specific date

  Scenario: Retrieve information of cultural events for a date
    Given I set header "HTTP_ACCEPT" with value "application/json"
    And I set header "CONTENT_TYPE" with value "application/json"
    When I send a GET request to "/agenda/2015-09-30"
    Then the response code should be 200
    And the response should contain "Euskal Herriko Meatzaritzaren Museoa (Erakusketa iraunkorra)"
