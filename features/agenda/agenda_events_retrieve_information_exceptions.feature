Feature: Recover agenda events information
  As a user
  I want to recover agenda events for an specific date

  Scenario: Retrieve information of cultural events for a date in the past
    Given I set header "HTTP_ACCEPT" with value "application/json"
    And I set header "CONTENT_TYPE" with value "application/json"
    When I send a GET request to "/agenda/2009-09-30"
    Then the response code should be 404


  Scenario: Retrieve information of cultural events for an invalid date
    Given I set header "HTTP_ACCEPT" with value "application/json"
    And I set header "CONTENT_TYPE" with value "application/json"
    When I send a GET request to "/agenda/30-09-2015"
    Then the response code should be 409
